import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  constructor(private http: HttpClient) {
  }

  getTeamOfProject(projectId: number): Observable<any> {
    return this.http.get(`${environment.teamsProject}/${projectId}`);
  }

  getAProjectWhereUserExist(id: number): Observable<any> {
    return this.http.get(`${environment.teamsConnecting}/${id}`);
  }

  addTeamToProject(id, maintainers, developers) {
    const body = {
      project: id,
      maintainers,
      developers
    };

    return this.http.post(`${environment.teamsAddTeamMates}`, body);
  }

}
